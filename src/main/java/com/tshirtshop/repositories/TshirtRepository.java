package com.tshirtshop.repositories;

import com.tshirtshop.entities.Tshirt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TshirtRepository extends JpaRepository<Tshirt, Long> {

}
