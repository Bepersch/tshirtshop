package com.tshirtshop.entities;

import javax.persistence.*;

@Entity
@Table(name = "tshirt")
public class Tshirt {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String link;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user")
    private User user;


    public Tshirt() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
